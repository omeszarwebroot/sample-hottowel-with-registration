using Mvc.Mailer;
using HotTowelWithUserReg.Mailers.Models;

namespace HotTowelWithUserReg.Mailers
{ 
    public interface IPasswordResetMailer
    {
			MvcMailMessage PasswordReset(MailerModel model);
	}
}